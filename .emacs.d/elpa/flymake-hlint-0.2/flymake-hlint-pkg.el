(define-package "flymake-hlint" "0.2" "A flymake handler for haskell-mode files using hlint" '((flymake-easy "0.1")) :url "https://github.com/purcell/flymake-hlint")
